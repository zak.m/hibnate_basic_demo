package hib_anz_batch1_demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
	
	public static void main(String[] args) {
		
		Configuration cfg=new Configuration();
		cfg.configure("hibernate.cfg.xml");
		
		SessionFactory sf=cfg.buildSessionFactory();
		
		Session ses=sf.getCurrentSession();
		
		
		Emolyee em=new Emolyee(12,"batman", 100);
		
		ses.beginTransaction();
		
		ses.save(em);
		ses.getTransaction().commit();
		
		System.out.println("bye");
		
	}

}

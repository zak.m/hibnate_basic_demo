package hib_anz_batch1_demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Emolyee {
	
	@Id
	private int eid;
	@Column(length = 30)
	private String name;
	
	private int salay;
	
	public Emolyee() {}
	
	public Emolyee(int eid, String name, int salay) {
		super();
		this.eid = eid;
		this.name = name;
		this.salay = salay;
	}
	public int getEid() {
		return eid;
	}
	public void setEid(int eid) {
		this.eid = eid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSalay() {
		return salay;
	}
	public void setSalay(int salay) {
		this.salay = salay;
	}
	@Override
	public String toString() {
		return "Emolyee [eid=" + eid + ", name=" + name + ", salay=" + salay + "]";
	}
	
	

}
